# REHAB DEMO #

Jigsaw Demo

### What has been used? ###

* Yo
* Bower
* NPM
* Angular 1.4
* Angular Material
* ngDraggable
* Business Cat
* Lodash
* SASS

### How do I get set up? ###

* Clone it down
* npm install && bower install
* gulp serve

### How does it work ###

* Two grids of 9 squares - first set segments of business cat - second set transparent
* Only 'solve' grid allows dropping.
* On drop, swap squares, check for victory.