(function() {
  'use strict';

  angular
    .module('rehab')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
