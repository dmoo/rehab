(function() {
  'use strict';

  angular
    .module('rehab')
    .controller('JigsawController', JigsawController);

  /** @ngInject */
  function JigsawController($scope, lodash) {
    var vm = this;

    vm.pieces = {
      "jumble": [],
      "solved": []
    };
    vm.winningPattern = [];
    vm.hasWon = false;

    // Generate initial stuff
    for (var i = 1; i <= 9; ++i) {
      vm.winningPattern.push({style: 'piece__' + i});
      vm.pieces.jumble.push({style: 'piece__' + i});
      vm.pieces.solved.push({style: 'piece__mystery'});
    }

    //Jumble!
    vm.pieces.jumble = lodash.shuffle(vm.pieces.jumble);

    // Listen for drop
    $scope.onDropComplete=function(data, evt, index){
      // Switch the pieces
      if(data.comingFrom === 'jumble'){
        vm.pieces.jumble[data.index] = [vm.pieces.solved[index],
          vm.pieces.solved[index] = vm.pieces.jumble[data.index]][0];
      }else{
        vm.pieces.solved[index] = [vm.pieces.solved[data.index],
          vm.pieces.solved[data.index] = vm.pieces.solved[index]][0];
      }
      vm.checkFTW();
    }

    // FTW
    vm.checkFTW = function(){
      vm.hasWon = angular.equals(vm.winningPattern, vm.pieces.solved);
    };

  }
})();
