(function() {
  'use strict';

  angular
    .module('rehab', ['ngResource', 'ngRoute', 'ngMaterial', 'ngDraggable', 'ngLodash']);

})();
