(function() {
  'use strict';

  angular
    .module('rehab')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/jigsaw', {
        templateUrl: 'app/jigsaw/jigsaw.html',
        controller: 'JigsawController',
        controllerAs: 'jigsaw'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
